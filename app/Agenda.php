<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    protected $table = "agendas";

    protected $fillable = [
        'nome',
        'fones_res',
        'fone_cel',
        'dt_nasc',
        'facebook',
        'twitter',
        'instagram',
        'email'
    ];

    protected $dates = [
        'dt_nasc',
        'created_at',
        'updated_at',
    ];
}
